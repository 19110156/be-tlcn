import dotenv from 'dotenv'
import express, {urlencoded} from 'express'
import morgan from 'morgan'
import bodyParser from "body-parser"
import cors from 'cors'
import indexRoutes from './router/indexRoutes'
import {ConnectDB} from "./config/ConnectDB"
import {ExpressPeerServer} from 'peer'
import ErrorHandle from "./middleware/ErrorHandle";
dotenv.config()
const app=express()
app.use(bodyParser.urlencoded({extended:true}))
const http=require('http')
let server = http.createServer(app)
const io=require('socket.io')(server,
    {
        cors:{
            origin:"http://localhost:3000",
            method:["POST","GET"]
        }
    })
ExpressPeerServer(server,{path:"/"})

const corsOptions = {
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH'],
    "origin": "*",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
};
app.use(cors(corsOptions));
//middleware
app.use(morgan('dev'))
app.use(express.json());
app.use(express.urlencoded())
//routes
app.use('/api/v1/',indexRoutes)
app.all("*");
app.use(ErrorHandle)

//start server listening
const PORT=process.env.PORT||5000;
ConnectDB().then(()=>{
    console.log("")
})
server.listen(PORT,()=>{
    console.log(`app listening on port ${PORT}`);
})


