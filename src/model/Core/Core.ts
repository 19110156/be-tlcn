import mongoose from "mongoose"

const CoreSchema=new mongoose.Schema({
        name_topic:{type:String, required:true,ref:"topic"},
        user:{type:String, required:true,ref:"auth"},
        student_work:{type:Number, required:true},
        task:{type:Number, required:true},
        feedback:{type:Number, required:true}
    },{timestamps:true}
)
const CoreModels= mongoose.model('core',CoreSchema)
export default  CoreModels
module.exports=CoreModels

