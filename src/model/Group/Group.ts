import mongoose from "mongoose"

const GroupSchema=new mongoose.Schema({
        type:{type:String, required:true, enum:["student","teacher"]},
        users:[{type:String, required:true,ref:"auth"}],
    },{timestamps:true}
)
const GroupModels= mongoose.model('group',GroupSchema)
export default  GroupModels

