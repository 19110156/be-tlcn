import mongoose from "mongoose"

const AssemblySchema=new mongoose.Schema({
        name_topic:{type:String, required:true,ref:"topic"},
        group_teacher:{type:String, required:true,ref:"group"},
        group_student:{type:String, required:true,ref:"group"},
    },{timestamps:true}
)
const AssemblyModels= mongoose.model('assembly',AssemblySchema)
export default  AssemblyModels
module.exports=AssemblyModels

