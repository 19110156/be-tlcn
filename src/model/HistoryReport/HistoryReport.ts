import mongoose from "mongoose"

const HistoryReportSchema=new mongoose.Schema({
        name_task:{type:String, required:true},
        week:{type:String, required:true, unique:true},
        percent:{type:String, required:true, minlength:[6,"Minlength least 6 character.."]},
        status:{type:String, required:true, minlength:[6,"Minlength least 6 character.."]}
    },{timestamps:true}
)
const HistoryReportModels= mongoose.model('history_report',HistoryReportSchema)
export default  HistoryReportModels
module.exports=HistoryReportModels

