import mongoose from "mongoose"

const TopicSchema=new mongoose.Schema({
        name_topic:{type:String, required:true},
        des_topic:{type:String, required:true, },
        func:[{type:String}],
        task:[{type:String}],
        group_student:{type:String ,ref:"group"},
        group_teacher:{type:String,ref:"group"}
    }
)
const TopicModels= mongoose.model('topic',TopicSchema)
export default  TopicModels
module.exports=TopicModels

