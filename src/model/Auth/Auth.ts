import mongoose from "mongoose"

const AuthSchema=new mongoose.Schema({
        name:{type:String, required:true},
        email:{type:String, required:true, unique:true},
        password:{type:String, required:true, minlength:[6,"Minlength least 6 character.."]},
        role:{type:String, required:true,enum:["teacher","leader","student","admin"]},
        phone:{type:String, required:true,
            minlength:[10,"Minlength least 10 character.."],
            maxLength:[11,"Maxleng Phone 11 character"],
            unique:true
        }
    },{timestamps:true}
)
const AuthModels= mongoose.model('auth',AuthSchema)
export default  AuthModels
module.exports=AuthModels

