import express from 'express'
import {AuthMiddleware} from "../middleware/Auth"
import {AuthCtrl} from "../controller/Auth/AuthCtrl"

const routerAuth=express.Router()
routerAuth.get('/auth/all',AuthCtrl.findAllAuths)
routerAuth.post('/auth/forgot-password',AuthCtrl.forgotPassword)
routerAuth.post('/auth/reset-password',AuthCtrl.resetPassword)
routerAuth.post('/auth/login',[AuthMiddleware.login],AuthCtrl.login)
routerAuth.post('/auth/register',[AuthMiddleware.register],AuthCtrl.register)
routerAuth.put('/auth/resetPassword',AuthCtrl.resetPassword)
routerAuth.get('/refresh_token',AuthCtrl.generateAccessToken)
routerAuth.post('/auth/logout',AuthCtrl.logout)
routerAuth.delete('/auth/delete/:id',[AuthMiddleware.auth],AuthCtrl.deleteOneAccountById)

export default  routerAuth

