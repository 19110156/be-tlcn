import express from "express"
import TopicCtrl from "../controller/Topic/TopicCtrl"
import {AuthMiddleware} from "../middleware/Auth"
const routerTopic=express.Router()

routerTopic.post("/topic/add",[AuthMiddleware.auth],TopicCtrl.addTopic)
routerTopic.put("/topic/update/:id",[AuthMiddleware.auth],TopicCtrl.editTopic)
routerTopic.delete("/topic/delete/:id",[AuthMiddleware.auth],TopicCtrl.deleteTopic)
routerTopic.get("/topic",[AuthMiddleware.auth],TopicCtrl.getAllTopic)


export default routerTopic