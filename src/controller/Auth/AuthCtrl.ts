import {Response,Request} from "express"
const CryptoJS = require("crypto-js")
const jwt=require('jsonwebtoken')
// const nodemailer=require('nodemailer')
import nodemailer from 'nodemailer'
const AuthModels =require('../../model/Auth/Auth')

let secretKeyHash=`${process.env.PASSWORD_HASH_SECRET_KEY}`|| "abc"

export const AuthCtrl={
    findAllAuths: async (req:Request,res:Response)=>{
        try {
            const allUserResponse= await AuthModels.find({
                $or:[
                    {role:"member"}
                ],
                email:"duc@gmail.com"
            })
            return res.status(200).json({
                msg:"Find add User success !!",
                data:allUserResponse
            })
        }catch (err) {
            return res.status(500).json({msg:"Server Error", error:err.message})
        }
    },
    deleteOneAccountById: async (req:Request,res:Response)=>{
        try {
            const allUserResponse= await AuthModels.deleteOne({_id:req.params.id});
            return res.status(200).json({
                msg:"delete User success !!",
            })
        }catch (err) {
            return res.status(500).json({msg:"Server Error", error:err.message})
        }
    },
    login: async (req:Request,res:Response)=>{
        try {

            const {email,password}=req.body
            console.log({email,password})
            const user=await AuthModels.findOne({email:email})

            if(!user){
                return  res.status(500).json({msg:"User is not in DB"})
            }

            let bytes  = CryptoJS.AES.decrypt(user.password, secretKeyHash)

            let originalText = bytes.toString(CryptoJS.enc.Utf8)

            if(password!==originalText){

                return  res.status(500).json({msg:"Bạn đã sai password"})
            }
            const access_token = createAccessToken({id: user._id})
            const refresh_token = createRefreshToken({id: user._id})

            res.cookie('refreshtoken', refresh_token, {
                httpOnly: true,
                path: '/api/v1/refresh_token',
                maxAge: 30*24*60*60*1000 // 30days
            })

            return  res.status(200).json({
                msg: 'Login Success!',
                access_token,
                user: {
                    ...user._doc,
                    password: ''
                }
            })
        }catch (err) {
            return res.status(500).json({msg:"Server Error", error:err.message})
        }
    },
    register: async (req:Request,res:Response)=>{
        try {
            const {email,password,name,addressUser,role,phone}=req.body

            const user=await AuthModels.findOne({email:email})

            if(user){return  res.status(500).json({msg:"User đã tồn tại vui lòng sử dụng email khác !!!"})}

            let passwordHash = CryptoJS.AES.encrypt(password, secretKeyHash).toString()

            let userNew=new AuthModels({email,password:passwordHash,name,addressUser,role,phone})

            const access_token = createAccessToken({id: userNew._id})
            const refresh_token = createRefreshToken({id: userNew._id})

            res.cookie('refreshtoken', refresh_token, {
                httpOnly: true,
                path: '/api/v1/refresh_token',
                maxAge: 30*24*60*60*1000 // 30days
            })
            userNew.save().then((result:any)=>console.log(result))

            return  res.status(200).json({
                msg: 'Register Success!',
                access_token,
                user: {
                    ...userNew._doc,
                    password: ''
                }
            })
        }catch (err) {
            return res.status(500).json({msg:"Server Error", error:err.message})
        }
    },
    forgotPassword:async (req:Request,res:Response)=>{
        const {email}=req.body
        const user= await AuthModels.findOne({email:email})
        if(!user){
            return res.status(400).json({
                err:["Tài khoản chưa được đăng kí vui lòng kiểm tra lại !!!"]
            })
        }
        let mailOptions = {
            from: 'hongduc7754@gmail.com',
            to: `${email}`,
            subject: 'Sending Email using Node.js',
            html: '<h1>Welcome</h1><p>That was easy!</p>'
        }
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'hongduc7754@gmail.com',
                pass: '056240556'
            }
        });
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

    },
    resetPassword: async (req:Request,res:Response)=>{
        try {
            const{password,oldPassword,email}=req.body;
            const userUpdate= await AuthModels.findOne({email:email});
            let bytes  = CryptoJS.AES.decrypt(userUpdate.password, secretKeyHash);
            let originalText = bytes.toString(CryptoJS.enc.Utf8);
            if(oldPassword!==originalText){
                return  res.status(500).json({msg:"Password hien tai ban da nhap sai"});
            }
            let passwordHash = CryptoJS.AES.encrypt(password, secretKeyHash).toString();
            await AuthModels.findOneAndUpdate({email:email},{password:passwordHash},{new:true})
            return res.status(200).json({
                msg:"Find add User success !!",
                data:"allUserResponse"
            })

        }catch (err:any) {
            return res.status(500).json({msg:"Server Error", error:err.message})
        }
    },
    logout: async (req:Request,res:Response) => {
        try {
            res.clearCookie('refreshtoken', {path: '/api/v1/refresh_token'})
            return res.json({msg: "Logged out!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    generateAccessToken: async (req:Request,res:Response) => {
        try {

            const rf_token = req.cookies.refreshtoken

            if(!rf_token) return res.status(400).json({msg: "Please login now."})

            jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET, async(err:any, result:any) => {
                if(err) return res.status(400).json({msg: "Please login now."})

                const user = await AuthModels.findById(result.id)

                if(!user) return res.status(400).json({msg: "This does not exist."})

                const access_token = createAccessToken({id: result.id})

                res.json({
                    access_token,
                    user
                })
            })

        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },


}
const createAccessToken = (payload) => {
    return jwt.sign(payload, `${process.env.ACCESS_TOKEN_SECRET}`, {expiresIn: '1d'})
}
const createRefreshToken = (payload) => {
    return jwt.sign(payload,`${ process.env.REFRESH_TOKEN_SECRET}`, {expiresIn: '30d'})
}

















